# Q. 番組表をスクレイピングしたいです。

# A. 犬を飼うといいですよ



## !!!注意!!! ###

適当にスクレイピングしているので

番組が抜け落ちてても責任取りません。


## これはwhat's？ ###

番組表スクレイピング用ライブラリ

[AgpTimeTableLib](https://bitbucket.org/libraplanet/agptimetablelib)

のサンプル プロジェクトです。

ライブラリはデフォルトの出力を持っていません。

ライブラリをこねくり回して好きにフロント エンドをこさえればいいじゃんね


あと、サンプルついでにWPFでGUIフロント エンドを作ってみました。


## AgpTimeTableWpf ###
![SS_00_v0.0.0.0.0.0.0.0.0.0.0.0.0.0...2.png](https://bitbucket.org/libraplanet/agptimetable/raw/59fd9cab3510ea5a4c00cf1ecde89dec38c1d2df/images/SS_00_v0.0.0.0.0.0.0.0.0.0.0.0.0.0...2.png)


番組表をDLしてスクレイピングしてタブ区切りで出力します。

※連発するとサーバーに負荷をかけてしまうので、注意してください。



## library ###

* [AgpTimeTableLib](https://bitbucket.org/libraplanet/agptimetablelib)

* [Avalon Editor (icsharpcode) ](http://avalonedit.net/)

## special thanx ###

https://bitbucket.org/awazo/