﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AgpTimeTable
{
    class Program
    {
        class PrgRef
        {
            public bool Top { get; set; }
            public Prg Prg { get; set; }
        }

        class Prg
        {
            public int Week { get; set; }
            public int StartHour { get; set; }
            public int StartMin { get; set; }
            public int MinLength { get; set; }
            public string Title { get; set; }
            public string RP { get; set; }
        }
        static void Main(string[] args)
        {
            try
            {
                //string fname = args[0];
                string fname = "../../../../../material/html.html";
                using (FileStream fStream = new FileStream(fname, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    PrgRef[,] allList = read(fStream);
                    int timeMax = allList.GetLength(0);
                    int weekMax = allList.GetLength(1);
                    string[] weekTbl = new string[] { "月", "火", "水", "木", "金", "土", "日" };
                    //foreach (List<PrgRef> weekList in allList)
                    //{
                    //    weekMax = Math.Max(weekMax, weekList.Count);
                    //}
                    for (int i = 0; i < weekMax; i++)
                    {
                        for (int j = 0; j < timeMax; j++)
                        {
                            PrgRef prgRef = allList[j, i];
                            if(prgRef.Top)
                            {
                                Prg p = prgRef.Prg;
                                string index = string.Format("[{0:00},{1:00}]", new object[] { i, j });
                                string week = weekTbl[p.Week];
                                string time = string.Format("{0:00}:{1:00}", new object[] { p.StartHour, p.StartMin });
                                string minuits = string.Format("{0}", new object[] { p.MinLength }); ;
                                string title = p.Title;
                                //string line = string.Join("\t", new string[] { week, time, minuits, title });
                                string line = string.Join("\t", new string[] { index, week, time, minuits, title });
                                Console.WriteLine(line);
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        private static PrgRef[,] read(Stream stream)
        {
            string strHtml, strTbody;
            int weekMax = 7;
            int timeMax = (((4 + 24) - 6) * 2) + 1;
            PrgRef[,] chlist = new PrgRef[timeMax, weekMax];
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            {
                strHtml = reader.ReadToEnd();
            }
            Func<string, string, string> remove = (string str, string removeStr) =>
            {
                int index = str.IndexOf(removeStr);
                if (index < 0)
                {
                    return str;
                }
                else
                {
                    string str0 = str.Substring(0, index);
                    string str1 = str.Substring(index + removeStr.Length);
                    return str0 + str1;
                }
            };
            Func<string, string, string, string, bool, string> subTagBlock = (string str, string def, string strStart, string strEnd, bool isTrim) =>
            {
                int startIndex, endIndex, length;
                string ret = null;
                startIndex = str.IndexOf(strStart);
                if (startIndex >= 0)
                {
                    endIndex = str.IndexOf(strEnd, startIndex + strStart.Length);
                    if (endIndex >= 0)
                    {
                        length = (endIndex - startIndex) + strEnd.Length;
                        ret = str.Substring(startIndex, length);
                        if (isTrim)
                        {
                            ret = remove(ret, strStart);
                            ret = remove(ret, strEnd);
                        }
                        return ret;
                    }
                }
                return def;
            };
            Func<string, string, string, string> subTagBlock2 = (string str, string strStart, string strEnd) =>
            {
                int iStart = str.IndexOf(strStart);
                int iEnd = str.IndexOf(strEnd);
                if (iEnd < 0)
                {
                    return null;
                }
                else
                {
                    int length;
                    string ret;
                    iEnd += strEnd.Length;
                    if ((iStart < 0) || (iStart > iEnd))
                    {
                        iStart = 0;
                    }
                    length = iEnd - iStart;
                    ret = str.Substring(iStart, length);
                    return ret;
                }
            };
            //tbody
            {
                string comment;
                string strTmp = strHtml;
                strTmp = strTmp.Substring(strHtml.IndexOf("<div class=\"header-play\">"));
                strTbody = subTagBlock(strTmp, null, "<tbody class=\"scrollBody", "</tbody>", false);
                //コメント除去
                while (!string.IsNullOrEmpty(comment = subTagBlock(strTbody, null, "<!--", "-->", false)))
                {
                    strTbody = remove(strTbody, comment);
                }
            }
            //td
            {
                Action<string> collectTr = null;
                Action<string, int> collectTd = null;
                Action<string, int> collectCh = null;
                collectTr = (string str) =>
                {
                    int row = 0;
                    while (true)
                    {
                        string s = subTagBlock2(str, "<tr", "</tr>");
                        if (string.IsNullOrEmpty(s))
                        {
                            break;
                        }
                        else
                        {
                            str = remove(str, s);
                            collectTd(s, row);
                        }
                        row++;
                    };
                };

                collectTd = (string str, int row) =>
                {

                    while (true)
                    {
                        string s = subTagBlock2(str, "<td", "</td>");
                        if (string.IsNullOrEmpty(s))
                        {
                            break;
                        }
                        else
                        {
                            str = remove(str, s);
                            collectCh(s, row);
                        }
                    };
                };

                collectCh = (string str, int row) =>
                {
                    int rowspan = 1;
                    int week;
                    string th = subTagBlock(str, null, "<td ", ">", true);
                    string divTime = subTagBlock(str, null, "<div class=\"time\">", "</div>", true);
                    string divTitle = subTagBlock(str, null, "<div class=\"title-p\">", "</div>", true);
                    string divRp = subTagBlock(str, null, "<div class=\"rp\">", "</div>", true);
                    Prg prg = new Prg();
                    //week
                    {
                        int i = 0;
                        for(; i < weekMax; i++)
                        {
                            if(chlist[row, i] == null)
                            {
                                break;
                            }
                        }
                        week = i;
                    }
                    //rownum
                    {
                        string tmp = th;
                        string strRowspan = subTagBlock(str, null, "rowspan=\"", "\"", true);
                        if (!string.IsNullOrEmpty(strRowspan))
                        {
                            rowspan = int.Parse(strRowspan);
                        }
                        prg.MinLength = rowspan * 30;
                    }
                    //time
                    {
                        string tmp = divTime;
                        string span = subTagBlock(tmp, null, "<span>", "</span>", false);
                        string[] times;
                        int hour = 0;
                        int minute = 0;
                        if (!string.IsNullOrEmpty(span))
                        {
                            tmp = remove(tmp, span);
                        }
                        tmp = tmp.Trim();
                        times = tmp.Split(':');
                        if (times != null)
                        {
                            if (times.Length > 0)
                            {
                                hour = int.Parse(times[0]);
                            }
                            if (times.Length > 1)
                            {
                                minute = int.Parse(times[1]);
                            }
                        }
                        prg.StartHour = hour;
                        prg.StartMin = minute;
                        prg.Week = week;
                    }
                    //title
                    {
                        string tmp = divTitle;
                        string a = subTagBlock(tmp, null, "<a href=\"", ">", false);
                        if (!string.IsNullOrEmpty(a))
                        {
                            tmp = remove(tmp, a);
                            tmp = remove(tmp, "</a>");
                        }
                        tmp = tmp.Trim();
                        prg.Title = tmp;
                    }
                    //Rp
                    {
                        string tmp = divRp;
                        tmp = tmp.Trim();
                        prg.RP = tmp;
                    }

                    for(int i = 0; i < rowspan; i++)
                    {
                        int j = row + i;
                        PrgRef prgRef = new PrgRef() { Top = (i == 0), Prg = prg };
                        chlist[j, week] = prgRef;
                    }
                };
                collectTr(strTbody);
            }
            return chlist;
        }


        private static List<Prg> readXml(Stream s)
        {
            List<Prg> list = new List<Prg>();
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            settings.IgnoreComments = true;
            settings.IgnoreProcessingInstructions = true;
            settings.DtdProcessing = DtdProcessing.Parse;
            settings.ValidationType = ValidationType.None;
            settings.ValidationFlags = XmlSchemaValidationFlags.None;
            settings.ConformanceLevel = ConformanceLevel.Document;
            settings.ValidationEventHandler += (object sender, ValidationEventArgs e) =>
            {
                Console.WriteLine("Validation Error: {0}", e.Message);
            };
            using(XmlReader reader = XmlReader.Create(s, settings))
            {
                while (reader.Read())
                {
                    Console.WriteLine("[{0}]", reader.NodeType);
                    Console.WriteLine("name:{0}", reader.Name);
                    Console.WriteLine("value:{0}", reader.Value);
                    Console.WriteLine("--");
                    Console.WriteLine();
                }
            }
            return list;
        }
    }
}
