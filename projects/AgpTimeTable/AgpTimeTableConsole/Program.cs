﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AgpTimeTableLib;

namespace AgpTimeTableConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //string fname = args[0];
                string fname = "../../../../../material/html.html";
                using (FileStream fStream = new FileStream(fname, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    ChannelDto[][] allList = ChannelDto.Read(fStream);
                    int weekMax = allList.Length;
 
                    for (int j = 0; j < weekMax; j++)
                    {
                        ChannelDto[] list = allList[j];
                        int timeMax = list.Length;
                        for (int i = 0; i < timeMax; i++)
                        {
                            ChannelDto ch = list[i];
                            string index = string.Format("[{0:00},{1:00}]", new object[] { j, i });
                            string week = ch.VirtualWeekName;
                            string time = string.Format("{0:00}:{1:00}", new object[] { ch.RealStartHour, ch.StartMin });
                            string minuits = string.Format("{0}", new object[] { ch.MinLength }); ;
                            string title = ch.Title;
                            //string line = string.Join("\t", new string[] { week, time, minuits, title });
                            string line = string.Join("\t", new string[] { index, week, time, minuits, title });
                            Console.WriteLine(line);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
