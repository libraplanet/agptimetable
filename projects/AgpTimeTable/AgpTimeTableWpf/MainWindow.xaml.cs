﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace AgpTimeTableWpf
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        class ChannelDto : ICloneable
        {
            public int virtualWeekIndex;
            public string virtualWeekName;
            public int virtualStartHour;
            public int realWeekIndex;
            public string realWeekName;
            public int realStartHour;
            public int startMin;
            public int minLength;
            public string url;
            public string mailAddress;
            public string rp;
            public string title;
            public string srcRP;

            public ChannelDto()
            {

            }
            public ChannelDto(ChannelDto o)
            {
                virtualWeekIndex = o.virtualWeekIndex;
                virtualWeekName = o.virtualWeekName;
                virtualStartHour = o.virtualStartHour;
                realWeekIndex = o.realWeekIndex;
                realWeekName = o.realWeekName;
                realStartHour = o.realStartHour;
                startMin = o.startMin;
                minLength = o.minLength;
                url = o.url;
                mailAddress = o.mailAddress;
                rp = o.rp;
                title = o.title;
                srcRP = o.srcRP;
            }

            public ChannelDto(AgpTimeTableLib.ChannelDto o)
            {
                virtualWeekIndex = o.VirtualWeekIndex;
                virtualWeekName = o.VirtualWeekName;
                virtualStartHour = o.VirtualStartHour;
                realWeekIndex = o.RealWeekIndex;
                realWeekName = o.RealWeekName;
                realStartHour = o.RealStartHour;
                startMin = o.StartMin;
                minLength = o.MinLength;
                url = o.Url;
                mailAddress = o.MailAddress;
                rp = o.RP;
                title = o.Title;
                srcRP = o.SrcRP;
            }

            public object Clone()
            {
                return new ChannelDto(this);
            }
        }

        string cacheDirName = "cache";
        string cacheFname = "cache.html";
        string cacheDirPath;
        string cacheFilePath;

        private string ApplicationPath
        {
            get
            {
                return System.Reflection.Assembly.GetExecutingAssembly().Location;
            }
        }
        private string ApplicationDir
        {
            get
            {
                return System.IO.Path.GetDirectoryName(ApplicationPath);
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            cacheDirPath = System.IO.Path.Combine(ApplicationDir, cacheDirName);
            cacheFilePath = System.IO.Path.Combine(cacheDirPath, cacheFname);
        }

        #region [event]
        private void window_Initialized(object sender, EventArgs e)
        {
            fixStateStyle();
        }
        private void window_StateChanged(object sender, EventArgs e)
        {
            fixStateStyle();
        }
        private void buttonCache_Click(object sender, RoutedEventArgs e)
        {
            c();
        }

        private void buttonProcess_Click(object sender, RoutedEventArgs e)
        {
            s();
        }
        private void textboxWeekPadCnt_LostFocus(object sender, RoutedEventArgs e)
        {
            forceNumber(sender);
        }
        private void textboxWeekPadCnt_TextChanged(object sender, TextChangedEventArgs e)
        {
            //forceNumber(sender);
        }
        #endregion

        #region [command]
        private void CommandCloseCommand(object sender, RoutedEventArgs e)
        {
            SystemCommands.CloseWindow(this);
        }
        private void CommandMaximizeWindow(object sender, RoutedEventArgs e)
        {
            SystemCommands.MaximizeWindow(this);
        }
        private void CommandMinimizeWindow(object sender, RoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }
        private void CommandRestoreWindow(object sender, RoutedEventArgs e)
        {
            SystemCommands.RestoreWindow(this);
        }
        #endregion

        #region[methods]
        private void fixStateStyle()
        {
            if (this.WindowState == System.Windows.WindowState.Maximized)
            {
                BorderThickness = new Thickness(6);
                this.buttonNormalize.Visibility = System.Windows.Visibility.Visible;
                this.buttonMaximize.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                BorderThickness = new Thickness(0);
                this.buttonNormalize.Visibility = System.Windows.Visibility.Collapsed;
                this.buttonMaximize.Visibility = System.Windows.Visibility.Visible;
            }
        }
        private void activateControl(bool active)
        {
            textBoxUrl.IsEnabled = active;
            buttonCashe.IsEnabled = active;
            buttonProcess.IsEnabled = active;
            gridParams.IsEnabled = active;
            menuBar.IsEnabled = active;
        }
        private void forceNumber(object obj)
        {
            forceNumber(obj as TextBox);
        }
        private void forceNumber(TextBox textbox)
        {
            if(textbox != null)
            {
                if(textbox.Text != null)
                {
                    string str = textbox.Text;
                    int num = 0;
                    str = str.Trim();
                    str = str.Replace(",", "");
                    str = str.Replace(".", "");
                    str = str.Replace(" ", "");
                    int.TryParse(str, out num);
                    textbox.Text = num.ToString();
                }
            }
        }
        #endregion

        #region [menu]
        private void FileMenuOpen(object sender, RoutedEventArgs e)
        {
        }
        private void FileMenuSave(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "*|file";
            dialog.CheckFileExists = true;
            dialog.CheckPathExists = true;
            dialog.CreatePrompt = false;
            dialog.FilterIndex = 0;
            if(dialog.ShowDialog() == true)
            {
                using (FileStream fStream = new FileStream(dialog.FileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                using (StreamWriter sWriter = new StreamWriter(fStream, Encoding.UTF8))
                {
                    sWriter.Write(textBoxList.Text);
                }
            }
        }
        private void WindowMenuTopMost(object sender, RoutedEventArgs e)
        {
            this.Topmost = !this.Topmost;
            menuItemTopMost.IsChecked = this.Topmost;
        }
        private void HelpMenuVersion(object sender, RoutedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("{0}", this.Title));
            sb.AppendLine();
            sb.AppendLine(string.Format("  vsersion : {0}", "0.0.0.0.0.0.0.0.0.0.0.0.0.0....."));
            sb.AppendLine(string.Format("  auther   : {0}", "libraplanet"));
            MessageBox.Show(sb.ToString(), this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
        }
        #endregion

        private void caching(string url)
        {
            //string logChacheFname = string.Format("cache_{0}.html", new string[] { DateTime.Now.ToString("yyMMdd_HHmmss") });
            //string logChacheFilePath = System.IO.Path.Combine(cacheDirPath, logChacheFname);
            byte[] data;

            Action<string, string, string, int> cacheLotate = null;
            cacheLotate = (dir, baseFname, srcFname, cnt) =>
            {
                string srcPath = System.IO.Path.Combine(dir, srcFname);
                if (cnt > 100)
                {
                    File.Delete(srcPath);
                }
                else
                {
                    if (File.Exists(srcPath))
                    {
                        int next = cnt + 1;
                        string tgtFname = string.Format("{0}_{1:00}{2}", new object[]{
                            System.IO.Path.GetFileNameWithoutExtension(baseFname),
                            next,
                            System.IO.Path.GetExtension(baseFname),
                        });
                        string tgtPath = System.IO.Path.Combine(dir, tgtFname);

                        if (File.Exists(tgtPath))
                        {
                            cacheLotate(dir, baseFname, tgtFname, next);
                        }
                        File.Move(srcPath, tgtPath);
                    }
                }
            };

            //download
            using (WebClient wc = new WebClient() { Encoding = Encoding.UTF8 })
            {
                data = wc.DownloadData(url);
            }

            if (!Directory.Exists(cacheDirPath))
            {
                Directory.CreateDirectory(cacheDirPath);
            }

            //cache
            {
                cacheLotate(cacheDirPath, cacheFname, cacheFname, 0);

                using (FileStream fStream = new FileStream(cacheFilePath, FileMode.Create, FileAccess.ReadWrite))
                {
                    fStream.Write(data, 0, data.Length);
                }
            }
        }

        private string readCache()
        {
            using (FileStream fStream = new FileStream(cacheFilePath, FileMode.Open, FileAccess.ReadWrite))
            using (StreamReader reader = new StreamReader(fStream, Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }

        private void c()
        {
            try
            {
                string url = textBoxUrl.Text;
                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += (object sender, DoWorkEventArgs e) =>
                {
                    caching(url);
                    worker.ReportProgress(10, readCache());
                };

                worker.ProgressChanged += (object sender, ProgressChangedEventArgs e) =>
                {
                    if (e.ProgressPercentage == 10)
                    {
                        textBoxHtml.AppendText(string.Format("{0}\r\n", e.UserState as string));
                    }
                };

                worker.RunWorkerCompleted += (object sender, RunWorkerCompletedEventArgs e) =>
                {
                    activateControl(true);
                };

                activateControl(false);
                textBoxHtml.Text = "";
                textBoxList.Text = "";

                worker.WorkerReportsProgress = true;

                worker.RunWorkerAsync();
            }
            catch (Exception e)
            {
                textBoxList.Text = e.ToString();
            }
        }

        private void s()
        {
            try
            {
                string url = textBoxUrl.Text;
                bool isLineNo = checkboxLineNo.IsChecked == true;
                bool isLeveledProgram = checkboxLeveledProgram.IsChecked == true;

                bool isDateReal = radioDateReal.IsChecked == true;
                bool isDateVirtual = radioDateVirtual.IsChecked == true;
                bool isTimeFormatVE = radioTimeFormatVE.IsChecked == true;
                bool isTimeFormatVJ = radioTimeFormatVJ.IsChecked == true;
                bool isTimeFormatFE = radioTimeFormatFE.IsChecked == true;
                bool isTimeFormatFJ = radioTimeFormatFJ.IsChecked == true;
                bool isMinFormatE = radioMinFormatE.IsChecked == true;
                bool isMinFormatJ = radioMinFormatJ.IsChecked == true;
                int weekPadCnt = 0;

                if (checkboxWeekPad.IsChecked == true)
                {
                    if (textboxWeekPadCnt.IsEnabled)
                    {
                        int cnt;
                        string str = textboxWeekPadCnt.Text;
                        if (int.TryParse(str, out cnt))
                        {
                            weekPadCnt = Math.Max(weekPadCnt, cnt);
                        }
                    }
                }

                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += (object sender, DoWorkEventArgs e) =>
                {
                    using (WebClient wc = new WebClient() { Encoding = Encoding.UTF8 })
                    {
                        string html;
                        StringBuilder sb = new StringBuilder();

                        //cache or download
                        {
                            if (!File.Exists(cacheFilePath))
                            {
                                caching(url);
                            }
                            html = readCache();
                            worker.ReportProgress(10, html);
                        }

                        //parse
                        {
                            AgpTimeTableLib.ChannelDto[][] orgAllList = AgpTimeTableLib.ChannelDto.Read(html);
                            int weekMax = orgAllList.Length;
                            ChannelDto[][] allList;
                            string timeFormat;
                            string minFormat;

                            //rec
                            {
                                allList = new ChannelDto[orgAllList.Length][];
                                for(int j = 0; j < orgAllList.Length; j++)
                                {
                                    allList[j] = new ChannelDto[orgAllList[j].Length];
                                    for (int i = 0; i < orgAllList[j].Length; i++)
                                    {
                                        allList[j][i] = new ChannelDto(orgAllList[j][i]);
                                    }
                                }
                            }

                            if (isLeveledProgram)
                            {
                                for(int j = 0; j < allList.Length; j++)
                                {
                                    ChannelDto[] chList = allList[j];
                                    List<ChannelDto> newList = new List<ChannelDto>();

                                    for (int i = 0; i < chList.Length; i++)
                                    {
                                        ChannelDto chSrc = chList[i];
                                        ChannelDto chLeveled = new ChannelDto(chSrc);
                                        int indexLeveled = -1;

                                        int min0 = (chSrc.virtualStartHour * 60) + chSrc.startMin;
                                        for (int p = i + 1; p < chList.Length; p++)
                                        {
                                            ChannelDto chTgt = chList[p];
                                            int min1 = (chTgt.virtualStartHour * 60) + chTgt.startMin;
                                            if ((min1 - min0) < 60)
                                            {
                                                if(string.Equals(chSrc.title, chTgt.title))
                                                {
                                                    chLeveled.minLength = ((chTgt.virtualStartHour - chSrc.virtualStartHour) * 60) + chTgt.startMin + chTgt.minLength;
                                                    indexLeveled = p;
                                                }
                                            }
                                            else
                                            {
                                                break;
                                            }
                                        }

                                        if (indexLeveled < 0)
                                        {
                                            newList.Add(chSrc);
                                        }
                                        else
                                        {
                                            newList.Add(chLeveled);
                                            i = indexLeveled;
                                        }
                                    }
                                    allList[j] = newList.ToArray();
                                }
                            }

                            //timeformat
                            {
                                if (isTimeFormatVE)
                                {
                                    timeFormat = "{0:0}:{1:0}";
                                }
                                else if (isTimeFormatVJ)
                                {
                                    timeFormat = "{0:0}時{1:0}分";
                                }
                                else if (isTimeFormatFE)
                                {
                                    timeFormat = "{0:00}:{1:00}";
                                }
                                else if (isTimeFormatFJ)
                                {
                                    timeFormat = "{0:00}時{1:00}分";
                                }
                                else
                                {
                                    timeFormat = "";
                                }
                            }
                            //min length
                            {
                                if (isMinFormatE)
                                {
                                    minFormat = "{0:0}";
                                }
                                else if (isMinFormatJ)
                                {
                                    minFormat = "{0:0}分";
                                }
                                else
                                {
                                    minFormat = "";
                                }
                            }

                            for (int j = 0; j < weekMax; j++)
                            {
                                ChannelDto[] list = allList[j];
                                int timeMax = list.Length;
                                for (int i = 0; i < timeMax; i++)
                                {
                                    ChannelDto ch = list[i];
                                    string index;
                                    string weekName;
                                    string time;
                                    string minuits;
                                    string title;
                                    List<string> argsList = new List<string>();

                                    //create format
                                    {
                                        int startHour, startMin;

                                        if (isDateReal)
                                        {
                                            weekName = ch.realWeekName;
                                            startHour = ch.realStartHour;
                                            startMin = ch.startMin;
                                        }
                                        else if (isDateVirtual)
                                        {
                                            weekName = ch.virtualWeekName;
                                            startHour = ch.virtualStartHour;
                                            startMin = ch.startMin;
                                        }
                                        else
                                        {
                                            weekName = "";
                                            startHour = 0;
                                            startMin = 0;
                                        }

                                        index = string.Format("[{0:00},{1:00}]", new object[] { j, i });
                                        time = string.Format(timeFormat, new object[] { startHour, startMin });
                                        minuits = string.Format(minFormat, new object[] { ch.minLength }); ;
                                        title = ch.title;
                                    }
                                    //create list
                                    {
                                        if (isLineNo)
                                        {
                                            argsList.Add(index);
                                        }
                                        argsList.AddRange(new string[] { weekName, time, minuits, title });
                                    }
                                    //create line
                                    {
                                        string line = string.Join("\t", argsList.ToArray());
                                        sb.AppendLine(line);
                                        //worker.ReportProgress(1, line);
                                    }
                                }
                                for (int i = timeMax; i < weekPadCnt; i++)
                                {
                                    sb.AppendLine("");
                                }
                            }
                            //textBoxList.Text = sb.ToString();
                            worker.ReportProgress(11, sb.ToString());
                        }
                    }
                };

                worker.ProgressChanged += (object sender, ProgressChangedEventArgs e) =>
                {
                    if (e.ProgressPercentage == 0)
                    {
                        textBoxHtml.AppendText(string.Format("{0}\r\n", e.UserState as string));
                    }
                    else if (e.ProgressPercentage == 1)
                    {
                        textBoxList.AppendText(string.Format("{0}\r\n", e.UserState as string));
                    }
                    else if (e.ProgressPercentage == 10)
                    {
                        textBoxHtml.AppendText(string.Format("{0}\r\n", e.UserState as string));
                    }
                    else if (e.ProgressPercentage == 11)
                    {
                        textBoxList.AppendText(string.Format("{0}\r\n", e.UserState as string));
                    }
                };

                worker.RunWorkerCompleted += (object sender, RunWorkerCompletedEventArgs e) =>
                {
                    activateControl(true);
                };

                textBoxHtml.Text = "";
                textBoxList.Text = "";
                activateControl(false);

                worker.WorkerReportsProgress = true;

                worker.RunWorkerAsync();
            }
            catch (Exception e)
            {
                textBoxList.Text = e.ToString();
            }
        }
    }
}
